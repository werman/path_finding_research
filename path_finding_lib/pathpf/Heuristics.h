#pragma once

#include <algorithm>

#include "globals.h"

namespace pf
{

    //some useful template functions for creating heuristics for movement on a 2d plane
    //reference: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html

    struct ManhattanDistance
    {
        //The standard heuristic is the Manhattan distance.
        //Look at your cost function and see what the least cost is
        //for moving from one space to another.
        //The heuristic should be cost times manhattan distance:
        template<typename T>
        static const T nodes_distance( const T& currentX, const T& currentY, const T& targetX, const T& targetY,
                                       const T& startX,
                                       const T& startY ) // startX and startY are to be compatible with other functions call
        {
            return ( std::abs( (int32_t)currentX - (int32_t)targetX ) + std::abs( (int32_t)currentY - (int32_t)targetY ));
        }
    };

    struct StraightDistance
    {

        //If your units can move at any angle (instead of grid directions),
        //then you should probably use a straight line distance:
        template<typename T>
        static const T nodes_distance( const T& currentX, const T& currentY, const T& targetX, const T& targetY,
                                       const T& startX,
                                       const T& startY ) // startX and startY are to be compatible with other functions call
        {
            int32_t dx = ( (int32_t)currentX - (int32_t)targetX );
            int32_t dy = ( (int32_t)currentY - (int32_t)targetY );
            return ( std::sqrt( dx * dx + dy * dy ));
        }
    };

    struct TieBreakerDistance
    {

        //One thing that can lead to poor performance is ties in the heuristic.
        //When several paths have the same f value, they are all explored, even
        //though we only need to explore one of them. To solve this problem, we
        //can add a small tie-breaker to the heuristic.
        template<typename T>
        static const T nodes_distance( const T& currentX, const T& currentY, const T& targetX, const T& targetY,
                                       const T& startX, const T& startY )
        {
            int32_t dx1 = currentX - targetX;
            int32_t dy1 = currentY - targetY;
            int32_t dx2 = startX - targetX;
            int32_t dy2 = startY - targetY;
            int32_t cross = dx1 * dy2 - dx2 * dy1;
            if( cross < 0 )
            {
                cross = -cross;
            }
            return cross;
        }
    };


}
