#pragma once

#include <stdint.h>
#include <vector>

#include "globals.h"
#include "GoalBounding.h"

namespace pf
{
    class MapNode
    {
    public:
        MapNode()
        { }

        MapNode( uint32_t weight, PassabilityType type )
                : m_weight( weight ), m_type( type )
        { }

        inline
        uint32_t GetWeight() const
        {
            return m_weight;
        }

        inline
        void SetWeight( uint32_t weight )
        {
            m_weight = weight;
        }

        inline
        PassabilityType GetType() const
        {
            return m_type;
        }

        inline
        void SetType( PassabilityType type )
        {
            m_type = type;
        }

        inline
        bool IsPassable( PassabilityType type ) const
        {
            return ( m_type & type ) > 0;
        }

    private:
        uint32_t m_weight{ 0 };
        PassabilityType m_type{ 0 };
    };

    template<typename CoordsType>
    class SimpleMap
    {
    public:

        SimpleMap()
        { }

        SimpleMap( const std::string& name )
                : m_name( name )
        { }

        void Init( CoordsType sizeX, CoordsType sizeY )
        {
            m_sizeX = sizeX;
            m_sizeY = sizeY;
            m_nodes.resize( sizeX * sizeY );
        }

        void LoadGoalBounds( const std::string& folderPath = "" )
        {
            auto filePath = folderPath + m_name + ".gbounds";

            if( !m_name.empty())
            {
                m_goalBounds.reset( new GoalBounding());
                if( !m_goalBounds->Load( filePath ))
                {
                    m_goalBounds.reset( nullptr );
                }
            }
            else
            {
                m_goalBounds.reset( nullptr );
            }
        }

        void CalculateGoalBounds( const std::string& folderPath = "" )
        {
            m_goalBounds.reset( new GoalBounding());

            auto filePath = folderPath + m_name + ".gbounds";

            if(!m_name.empty())
            {
                m_goalBounds.reset( new GoalBounding());
                bool loaded = m_goalBounds->Load( filePath );
                if( !loaded || !m_goalBounds->IsValid( *this ) )
                {
                    m_goalBounds->Process( *this, 1 );
                    m_goalBounds->Save( filePath );
                }
            }
        }

        const std::string GetName() const
        {
            return m_name;
        }

        const GoalBounding* GetGoalBounds() const
        {
            return m_goalBounds.get();
        }

        inline
        CoordsType GetSizeX() const
        {
            return m_sizeX;
        }

        inline
        CoordsType GetSizeY() const
        {
            return m_sizeY;
        }

        inline
        MapNode& GetMutableNode( CoordsType x, CoordsType y )
        {
            return m_nodes[y * m_sizeX + x];
        }

        inline
        const MapNode& GetNode( CoordsType x, CoordsType y ) const
        {
            return m_nodes[y * m_sizeX + x];
        }

        PF_FUN_ENABLE_IF_UNSIGNED( CoordsType, bool )
        inline
        IsXOnMap( CoordsType x ) const
        {
            return x < m_sizeX;
        }

        PF_FUN_ENABLE_IF_SIGNED( CoordsType, bool )
        inline
        IsXOnMap( CoordsType x ) const
        {
            return x >= 0 && x < m_sizeX;
        }

        PF_FUN_ENABLE_IF_UNSIGNED( CoordsType, bool )
        inline
        IsYOnMap( CoordsType y ) const
        {
            return y < m_sizeY;
        }

        PF_FUN_ENABLE_IF_SIGNED( CoordsType, bool )
        inline
        IsYOnMap( CoordsType y ) const
        {
            return y >= 0 && y < m_sizeY;
        }

        inline
        bool IsNodeOnMap( CoordsType x, CoordsType y ) const
        {
            return IsXOnMap( x ) && IsYOnMap( y );
        }

        inline
        bool IsPassable( CoordsType x, CoordsType y, PassabilityType type ) const
        {
            return GetNode( x, y ).IsPassable( type );
        }

        inline
        bool IsPassableYSafe( CoordsType x, CoordsType y, PassabilityType type ) const
        {
            if( IsYOnMap( y ))
            {
                return GetNode( x, y ).IsPassable( type );
            }
            return false;
        }

        inline
        bool IsPassableXSafe( CoordsType x, CoordsType y, PassabilityType type ) const
        {
            if( IsXOnMap( x ))
            {
                return GetNode( x, y ).IsPassable( type );
            }
            return false;
        }

        inline
        bool IsPassableXYSafe( CoordsType x, CoordsType y, PassabilityType type ) const
        {
            if( IsXOnMap( x ) && IsXOnMap( y ))
            {
                return GetNode( x, y ).IsPassable( type );
            }
            return false;
        }

        inline
        bool operator()( unsigned x, unsigned y ) const
        {
            if( IsNodeOnMap( x, y ))
            {
                return IsPassable( x, y, 1 );
            }

            return false;
        }

    private:

        CoordsType m_sizeX{ 0 };
        CoordsType m_sizeY{ 0 };

        std::vector<MapNode> m_nodes;
        std::unique_ptr<GoalBounding> m_goalBounds{ nullptr };
        std::string m_name;
    };
}




