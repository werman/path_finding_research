#pragma once

template<bool C, typename T> using enable_if_t = typename std::enable_if<C,T>::type;

// Macros which can enable/disable method depending on template type
#define PF_FUN_ENABLE_IF( selector, val, returnType ) \
    template<class Q = selector>            \
    enable_if_t<std::is_same<Q, val>::value, returnType>

#define PF_FUN_ENABLE_IF_SIGNED( ValueType, ReturnType ) \
    template<class Q = ValueType> \
    enable_if_t<std::is_integral<Q>::value &&  std::is_signed<Q>::value, ReturnType>

#define PF_FUN_ENABLE_IF_UNSIGNED( ValueType, ReturnType ) \
    template<class Q = ValueType> \
    enable_if_t<std::is_integral<Q>::value &&  std::is_unsigned<Q>::value, ReturnType>

namespace pf
{
    using PassabilityType = uint32_t;
}
