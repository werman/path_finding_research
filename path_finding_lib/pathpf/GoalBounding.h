#pragma once

#include <cstdint>
#include <string>
#include <queue>
#include <map>
#include <math.h>
#include <fstream>
#include <boost/circular_buffer.hpp>

#include "threadpool11/threadpool11.hpp"

#include "globals.h"

namespace pf
{
    template<typename CoordsType>
    class SimpleMap;

class GoalBounding
{

public:

    enum class Direction
    {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        UP_LEFT,
        UP_RIGHT,
        DOWN_LEFT,
        DOWN_RIGHT,
        NONE
    };

    struct BoundingBox
    {
        uint16_t minX;
        uint16_t minY;
        uint16_t maxX;
        uint16_t maxY;

        inline bool Contains( uint16_t x, uint16_t y ) const
        {
            return x >= minX && x <= maxX && y >= minY && y <= maxY;
        }
    };

    struct NodeBBInfo
    {
        BoundingBox boundingBoxes[8];
    };

    struct BoundingBoxMap
    {
        void Init( pf::SimpleMap<uint16_t>& map );

        NodeBBInfo& GetNodeBB( uint16_t x, uint16_t y );

        const NodeBBInfo& GetNodeBB( uint16_t x, uint16_t y ) const;

        uint32_t m_sizeX;
        uint32_t m_sizeY;

        std::vector<NodeBBInfo> m_boundingBoxes;
    };

    struct FFMapNode
    {
        float cost;
        Direction originalDir;
    };

    struct FFMap
    {
        FFMap( pf::SimpleMap<uint16_t>& map );

        FFMapNode& GetNode( uint16_t x, uint16_t y );
        const FFMapNode& GetNode( uint16_t x, uint16_t y ) const;

        uint16_t m_sizeX;
        uint16_t m_sizeY;

        std::vector<FFMapNode> m_nodes;
        boost::circular_buffer<std::pair<uint32_t, uint32_t>> m_nodesQueue;
    };


public:

    void Process( pf::SimpleMap<uint16_t>& map, PassabilityType type );

    template<typename T>
    void read( std::ifstream& is, T& value )
    {
        is.read(( char* ) &value, sizeof( value ));
    }

    bool Load( std::string fileName )
    {
        std::ifstream infile( fileName, std::ifstream::binary );

        if( infile )
        {
            read( infile,m_bbMap.m_sizeX );
            read( infile,m_bbMap.m_sizeY );
            m_bbMap.m_boundingBoxes.resize( m_bbMap.m_sizeX * m_bbMap.m_sizeY );

            for( auto& nodeBBInfo: m_bbMap.m_boundingBoxes )
            {
                for( int i = 0; i < 8; i++ )
                {
                    auto& boundingBox = nodeBBInfo.boundingBoxes[i];
                    read( infile, boundingBox.minX );
                    read( infile, boundingBox.maxX );
                    read( infile, boundingBox.minY );
                    read( infile, boundingBox.maxY );
                }
            }
            infile.close();
            return true;
        }
        else
        {
            infile.close();
            return false;
        }
    }

    template<typename T>
    void write( std::ofstream& os, T value )
    {
        os.write(( char* ) &value, sizeof( T ));
    }

    bool Save( std::string fileName )
    {
        std::ofstream outfile( fileName, std::ofstream::binary );

        if( outfile )
        {
            write( outfile, m_bbMap.m_sizeX );
            write( outfile, m_bbMap.m_sizeY );

            for( const auto& nodeBBInfo: m_bbMap.m_boundingBoxes )
            {
                for( int i = 0; i < 8; i++ )
                {
                    const auto& boundingBox = nodeBBInfo.boundingBoxes[i];
                    write( outfile, boundingBox.minX );
                    write( outfile, boundingBox.maxX );
                    write( outfile, boundingBox.minY );
                    write( outfile, boundingBox.maxY );
                }
            }
            outfile.close();
            return true;
        }
        else
        {
            outfile.close();
            return false;
        }
    }

    bool IsValid( const pf::SimpleMap<uint16_t>& map ) const;

    bool IsInBound( uint16_t currentX, uint16_t currentY, uint16_t targetX, uint16_t targetY, Direction dir ) const;

    const NodeBBInfo& GetBBInfo( uint16_t x, uint16_t y ) const
    {
        return m_bbMap.GetNodeBB( x, y);
    }

    static void FloodFill( pf::SimpleMap<uint16_t>& map, FFMap& ffMap, uint16_t x, uint16_t y, PassabilityType type );

private:

    BoundingBoxMap m_bbMap;

};

}


