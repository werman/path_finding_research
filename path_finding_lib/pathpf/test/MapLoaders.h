#pragma once

#include <string>
#include <fstream>
#include <streambuf>
#include <cstdio>

#include "../globals.h"
#include "../Map.h"

enum TerrainType
{
    UNPASSABLE = 0,
    GROUND = 1,
    WATER = 1 << 1,
    SWAWMP = 1 << 2,
    FOREST = 1 << 3
};

namespace loaders
{
    std::string base_name( std::string const& path )
    {
        return path.substr( path.find_last_of( "/\\" ) + 1 );
    }

    std::string remove_extension( std::string const& filename )
    {
        typename std::string::size_type const p( filename.find_last_of( '.' ));
        return p > 0 && p != std::string::npos ? filename.substr( 0, p ) : filename;
    }

    template<typename CoordsType>
    pf::SimpleMap<CoordsType>* load_map_ai_lab_format( const std::string& filePath )
    {
        std::ifstream is( filePath );

        if( !is )
        {
            return nullptr;
        }

        std::string fileName = remove_extension( base_name( filePath ));

        std::string type, temp;
        is >> temp >> type;

        CoordsType width, height;

        is >> temp;
        if( temp == "width")
        {
            is >> width;
        }
        else
        {
            is >> height;
        }
        is >> temp;
        if( temp == "width")
        {
            is >> width;
        }
        else
        {
            is >> height;
        }

        auto map = new pf::SimpleMap<CoordsType>( fileName );

        map->Init( width, height );

        is >> temp;

        for( CoordsType i = 0; i < height; i++ )
        {
            is >> temp;

            if( temp.size() != width )
            {
                return nullptr;
            }

            for( CoordsType j = 0; j < width; j++ )
            {
                TerrainType terrainType;
                switch( temp[j] )
                {
                    case '.':
                    case 'G':
                        terrainType = TerrainType::GROUND;
                        break;
                    case '@':
                    case 'O':
                        terrainType = TerrainType::UNPASSABLE;
                        break;
                    case 'T':
                        terrainType = TerrainType::FOREST;
                        break;
                    case 'S':
                        terrainType = TerrainType::SWAWMP;
                        break;
                    case 'W':
                        terrainType = TerrainType::WATER;
                        break;
                    default:
                        terrainType = TerrainType::UNPASSABLE;
                }

                auto& node = map->GetMutableNode( j, i );
                node.SetType( static_cast<uint32_t>(terrainType));
                node.SetWeight( 1 );
            }
        }

        return map;
    }
}
