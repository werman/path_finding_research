#pragma once

#include <queue>                            // Doesn't support increasing priority
#include <boost/heap/priority_queue.hpp>    // Support increasing priority

#include "globals.h"

namespace pf
{

    /*
     * Open list data and functions
     */

    template<typename CoordsType>
    struct PriorityNode
    {
        PriorityNode( CoordsType x, CoordsType y, float priority )
                : x( x ), y( y ), priority( priority )
        { }

        bool Equals( CoordsType otherX, CoordsType otherY ) const
        {
            return ( x == otherX ) && ( y == otherY );
        }

        CoordsType x;
        CoordsType y;
        float priority;
    };

    template<typename CoordsType>
    struct JPSPriorityNode
    {
        JPSPriorityNode( CoordsType x, CoordsType y, uint32_t priority, short dx, short dy )
                : x( x ), y( y ), priority( priority ), dx( dx ), dy( dy )
        { }

        bool Equals( CoordsType otherX, CoordsType otherY ) const
        {
            return ( x == otherX ) && ( y == otherY );
        }

        CoordsType x;
        CoordsType y;
        uint32_t priority;

        short dx;
        short dy;
    };

    template<template<class> class NodeType, typename CoordsType>
    bool operator<( const NodeType<CoordsType>& lhs, const NodeType<CoordsType>& rhs )
    {
        return lhs.priority < rhs.priority;
    }

    template<template<class> class NodeType, typename CoordsType>
    bool operator>( const NodeType<CoordsType>& lhs, const NodeType<CoordsType>& rhs )
    {
        return lhs.priority > rhs.priority;
    }

    /////////////////////////////////

    uint32_t coords_to_contiguous_idx( uint16_t x, uint16_t y, uint16_t sizeX )
    {
        return y * sizeX + x;
    }

    int32_t coords_to_contiguous_idx( int16_t x, int16_t y, int16_t sizeX )
    {
        return y * sizeX + x;
    }

    std::tuple<uint16_t, uint16_t> contiguous_idx_to_coords( int32_t idx, uint16_t sizeX )
    {
        return std::make_tuple( idx % sizeX, idx / sizeX );
    }
}
