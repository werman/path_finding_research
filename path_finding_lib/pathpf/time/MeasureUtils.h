#pragma once

#include <cstdint>
#include <functional>
#include <chrono>

class MeasureUtils
{
public:

    class Settings
    {
    public:
        Settings( uint32_t callsCount, uint32_t warmupCallsCount )
                : m_callsCount( callsCount ), m_warmupCallsCount( warmupCallsCount )
        { }

        uint32_t getCallsCount() const
        {
            return m_callsCount;
        }

        uint32_t getWarmupCallsCount() const
        {
            return m_warmupCallsCount;
        }

    private:
        const uint32_t m_callsCount;
        const uint32_t m_warmupCallsCount;
    };

    template<typename T>
    struct MeasureResult
    {
        long time;
        long totalTime;
        T result;
    };

    template<typename TimeT, typename TClass, typename RetType, typename ... Args1, typename ... Args2>
    static MeasureResult<RetType> measureMethodEvalTime( Settings& settings, RetType (TClass::* func)( Args1... ),
                                                         TClass* context,
                                                         Args2&& ... args )
    {
        MeasureResult<RetType> result;

        // Call target method several times to make sure that some data is in cache.
        for( size_t i = 0; i < settings.getWarmupCallsCount(); i++ )
        {
            (( *context ).*func )( std::forward<Args2>( args )... );
        }

        auto startTime = std::chrono::high_resolution_clock::now();

        for( size_t i = 0; i < settings.getCallsCount(); i++ )
        {
            result.result = (( *context ).*func )( std::forward<Args2>( args )... );
        }

        auto endTime = std::chrono::high_resolution_clock::now();

        result.totalTime = std::chrono::duration_cast<TimeT>( endTime - startTime ).count();
        result.time = std::chrono::duration_cast<TimeT>(( endTime - startTime ) / settings.getCallsCount()).count();

        return result;
    }

    template<typename TimeT, typename TFunc, typename ... Args>
    static MeasureResult<std::result_of_t<TFunc( Args... )>> measureFunctionEvalTime( Settings& settings, TFunc&& func,
                                                                                      Args&& ... args )
    {
        MeasureResult<std::result_of_t<TFunc( Args... )>> result;

        // Call target method several times to make sure that some data is in cache.
        for( size_t i = 0; i < settings.getWarmupCallsCount(); i++ )
        {
            func( std::forward<Args>( args )... );
        }

        auto startTime = std::chrono::high_resolution_clock::now();

        for( size_t i = 0; i < settings.getCallsCount(); i++ )
        {
            result.result = func( std::forward<Args>( args )... );
        }

        auto endTime = std::chrono::high_resolution_clock::now();

        result.time = std::chrono::duration_cast<TimeT>(( endTime - startTime ) / settings.getCallsCount()).count();

        return result;
    }
};