#include <iostream>
#include "GoalBounding.h"
#include "Map.h"

using namespace pf;

void GoalBounding::BoundingBoxMap::Init( pf::SimpleMap<uint16_t>& map )
{
    m_sizeX = map.GetSizeX();
    m_sizeY = map.GetSizeY();
    m_boundingBoxes.resize( map.GetSizeX() * map.GetSizeY());
}

GoalBounding::NodeBBInfo& GoalBounding::BoundingBoxMap::GetNodeBB( uint16_t x, uint16_t y )
{
    return m_boundingBoxes[y * m_sizeX + x];
}

const GoalBounding::NodeBBInfo& GoalBounding::BoundingBoxMap::GetNodeBB( uint16_t x, uint16_t y ) const
{
    return m_boundingBoxes[y * m_sizeX + x];
}

GoalBounding::FFMap::FFMap( pf::SimpleMap<uint16_t>& map )
        : m_sizeX( map.GetSizeX()), m_sizeY( map.GetSizeY()), m_nodesQueue( map.GetSizeX() * map.GetSizeY() / 2 )
{
    m_nodes.resize( map.GetSizeX() * map.GetSizeY());
}

GoalBounding::FFMapNode& GoalBounding::FFMap::GetNode( uint16_t x, uint16_t y )
{
    return m_nodes[y * m_sizeX + x];
}

const GoalBounding::FFMapNode& GoalBounding::FFMap::GetNode( uint16_t x, uint16_t y ) const
{
    return m_nodes[y * m_sizeX + x];
}


void GoalBounding::Process( pf::SimpleMap<uint16_t>& map, uint32_t type )
{
    m_bbMap.Init( map );

    for( uint16_t i = 0; i < map.GetSizeX(); i++ )
    {
        for( uint16_t j = 0; j < map.GetSizeY(); j++ )
        {
            auto& bbNode = m_bbMap.GetNodeBB( i, j );

            for( int dir = 0; dir < 8; dir++ )
            {
                auto& bb = bbNode.boundingBoxes[dir];
                bb.minX = map.GetSizeX();
                bb.maxX = 0;
                bb.maxY = 0;
                bb.minY = map.GetSizeY();
            }
        }
    }

    threadpool11::Pool pool( 5 );

    std::map<std::thread::id, std::unique_ptr<FFMap>> ffMaps;   // We need one FFMap per thread
    std::mutex mapMutex;

    for( uint16_t i = 0; i < map.GetSizeX(); i++ )
    {
        for( uint16_t j = 0; j < map.GetSizeY(); j++ )
        {
            auto& node = map.GetNode( i, j );

            if( node.IsPassable( type ))
            {
                pool.postWork<void>( [ this, &map, &pool, &ffMaps, &mapMutex, i, j, type ]()
                                     {
                                         auto startX = i;
                                         auto startY = j;

                                         std::thread::id thisThreadId = std::this_thread::get_id();

                                         FFMap* ffMap{ nullptr };

                                         {
                                             std::lock_guard<std::mutex> lck( mapMutex );
                                             auto it = ffMaps.find( thisThreadId );
                                             if( it == ffMaps.end())
                                             {
                                                 it = ffMaps.insert( std::make_pair( thisThreadId,
                                                                                     std::make_unique<FFMap>(
                                                                                             map ))).first;
                                             }

                                             if( j % 50 == 0 )
                                             {
                                                 std::cout << "Row: " << i << " Col: " << j << "\n";
                                             }

                                             ffMap = it->second.get();
                                         }

                                         if( !map.IsPassable( startX, startY, type ))
                                         {
                                             return;
                                         }

                                         FloodFill( map, *ffMap, startX, startY, type );

                                         auto& bbNode = m_bbMap.GetNodeBB( startX, startY );

                                         for( uint16_t i = 0; i < map.GetSizeX(); i++ )
                                         {
                                             for( uint16_t j = 0; j < map.GetSizeY(); j++ )
                                             {
                                                 auto& ffNode = ffMap->GetNode( i, j );

                                                 if( map.IsPassable( i, j, type ))
                                                 {
                                                     auto& dirBB = bbNode.boundingBoxes[static_cast<int>(ffNode.originalDir)];

                                                     dirBB.minX = std::min( dirBB.minX, i );
                                                     dirBB.maxX = std::max( dirBB.maxX, i );
                                                     dirBB.minY = std::min( dirBB.minY, j );
                                                     dirBB.maxY = std::max( dirBB.maxY, j );
                                                 }
                                             }
                                         }
                                     } );
            }
        }
    }

    pool.waitAll();
}

bool GoalBounding::IsValid( const pf::SimpleMap<uint16_t>& map ) const
{
    return m_bbMap.m_sizeX == map.GetSizeX() && m_bbMap.m_sizeY == map.GetSizeY();
}

bool GoalBounding::IsInBound( uint16_t currentX, uint16_t currentY, uint16_t targetX, uint16_t targetY,
                              Direction dir ) const
{
    const auto& bbNode = m_bbMap.GetNodeBB( currentX, currentY );
    const auto& boundingBox = bbNode.boundingBoxes[static_cast<int>(dir)];
    return boundingBox.Contains( targetX, targetY );
}

void GoalBounding::FloodFill( pf::SimpleMap<uint16_t>& map, FFMap& ffMap, uint16_t x, uint16_t y, uint32_t type )
{
    for( uint16_t i = 0; i < map.GetSizeX(); i++ )
    {
        for( uint16_t j = 0; j < map.GetSizeY(); j++ )
        {
            auto& node = ffMap.GetNode( i, j );
            node.cost = 10000000.f;
            node.originalDir = Direction::NONE;
        }
    }

    auto& startNode = ffMap.GetNode( x, y );
    startNode.cost = 0.f;

    auto& nodesQueue = ffMap.m_nodesQueue;
    nodesQueue.clear();

#define CHECK_AND_ADD( x, y, dir )                                                      \
        if( static_cast<uint16_t>( x ) < map.GetSizeX() && static_cast<uint16_t>( y ) < map.GetSizeY() \
            && map.IsPassable( x, y, type ))   \
        {                                                                               \
            auto& node = ffMap.GetNode( x, y );                                         \
            node.cost = 1.f;                                                            \
            node.originalDir = dir;                                                     \
            nodesQueue.push_back( { x, y } );                                                \
        }

    CHECK_AND_ADD( x - 1, y - 1, Direction::UP_LEFT );
    CHECK_AND_ADD( x - 1, y, Direction::LEFT );
    CHECK_AND_ADD( x - 1, y + 1, Direction::DOWN_LEFT );
    CHECK_AND_ADD( x, y - 1, Direction::UP );
    CHECK_AND_ADD( x, y + 1, Direction::DOWN );
    CHECK_AND_ADD( x + 1, y - 1, Direction::UP_RIGHT );
    CHECK_AND_ADD( x + 1, y, Direction::RIGHT );
    CHECK_AND_ADD( x + 1, y + 1, Direction::DOWN_RIGHT );

#undef CHECK_AND_ADD


    struct Delta
    {
        short dx;
        short dy;
        float cost;
    };

    static Delta deltas[] = {{ -1, 0,  1.f },
                             { 1,  0,  1.f },
                             { 0,  -1, 1.f },
                             { 0,  1,  1.f },
                             { -1, -1, M_SQRT2 },
                             { 1,  -1, M_SQRT2 },
                             { -1, 1,  M_SQRT2 },
                             { 1,  1,  M_SQRT2 }};


    while( !nodesQueue.empty())
    {
        auto current = nodesQueue.front();
        nodesQueue.pop_front();

        auto& node = ffMap.GetNode( current.first, current.second );

        for( uint16_t i = 0; i < 8; i++ )
        {
            auto dx = deltas[i].dx;
            auto dy = deltas[i].dy;

            uint32_t nextX = current.first + dx;
            uint32_t nextY = current.second + dy;

            if( nextX < map.GetSizeX() && nextY < map.GetSizeY() && map.IsPassable( nextX, nextY, type ))
            {
                if( i >= 4 )
                {
                    if( !map.IsPassable( nextX - dx, nextY, type ) &&
                        !map.IsPassable( nextX, nextY - dy, type ))
                    {
                        continue;
                    }
                }

                auto& nextNode = ffMap.GetNode( nextX, nextY );

                auto newCost = node.cost + deltas[i].cost;
                if( nextNode.cost > newCost )
                {
                    nextNode.cost = newCost;
                    nextNode.originalDir = node.originalDir;
                    nodesQueue.push_back( { nextX, nextY } );
                }
            }
        }
    }
}
