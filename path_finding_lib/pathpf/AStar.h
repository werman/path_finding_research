#pragma once

#include <stdint.h>
#include <unordered_map>
#include <map>
#include "NodesStorage.h"
#include "Heuristics.h"
#include "Map.h"

#include "globals.h"

namespace pf
{
    template<typename CoordsType>
    struct Point
    {
        Point()
        { }

        Point( CoordsType x, CoordsType y )
                : x( x ), y( y )
        { }

        bool Equals( const Point<CoordsType>& other ) const
        {
            return x == other.x &&
                   y == other.y;
        }

        CoordsType x;
        CoordsType y;
    };

    template<typename CoordsType>
    bool operator==( const Point<CoordsType>& lhs, const Point<CoordsType>& rhs )
    {
        return lhs.x == rhs.x &&
               lhs.y == rhs.y;
    }

    ////////////////////////////
    /// Switches for options in pathfinding classes
    ////////////////////////////

    class EnableGoalBounding
    {
    };

    class DisableGoalBounding
    {
    };

    class EnableDbg
    {
    };

    class DisableDbg
    {
    };

    ////////////////////////////

    template<class CoordsType>
    class PathFinder
    {
    public:
        virtual ~PathFinder()
        { }

        virtual std::vector<Point<CoordsType>> find( CoordsType startX, CoordsType startY,
                                                     CoordsType targetX,
                                                     CoordsType targetY,
                                                     uint32_t unitType ) = 0;
    };

    template<typename CoordsType, class MapType, typename Heuritic, typename GoalBoundingStatus = DisableGoalBounding>
    class SimpleAStar : public PathFinder<CoordsType>
    {
        template<typename T>
        friend
        class DisplayedMap;

    private:

        using OpenListType = std::priority_queue<pf::PriorityNode<CoordsType>,
                std::vector<pf::PriorityNode<CoordsType>>, std::greater<pf::PriorityNode<CoordsType>>>;

        class NodeInfo
        {
        public:
            void SetVisited( bool visited )
            {
                m_visited = visited;
            }

            bool IsVisited() const
            {
                return m_visited;
            }

            void SetNodeCost( float nodeCost )
            {
                m_nodeCost = nodeCost;
            }

            uint32_t GetNodeCost() const
            {
                return m_nodeCost;
            }

            void SetCameFrom( CoordsType x, CoordsType y )
            {
                m_cameFrom.x = x;
                m_cameFrom.y = y;
            }

            const Point<CoordsType>& GetCameFrom() const
            {
                return m_cameFrom;
            }

        private:
            bool m_visited{ false };
            float m_nodeCost{ 0.f };
            Point<CoordsType> m_cameFrom;
        };

        const MapType& m_map;
        std::vector<NodeInfo> m_nodesInfo;

    public:

        SimpleAStar( const MapType& map )
                : m_map( map )
        {
            m_nodesInfo.resize( m_map.GetSizeX() * m_map.GetSizeY());
        }

        virtual ~SimpleAStar()
        { }

        PF_FUN_ENABLE_IF( GoalBoundingStatus, EnableGoalBounding, bool )
        TestGoalBounding( CoordsType currentX, CoordsType currentY, CoordsType targetX, CoordsType targetY,
                          GoalBounding::Direction dir )
        {
            return m_map.GetGoalBounds()->IsInBound( currentX, currentY, targetX, targetY, dir );
        }

        PF_FUN_ENABLE_IF( GoalBoundingStatus, DisableGoalBounding, bool )
        TestGoalBounding( CoordsType currentX, CoordsType currentY, CoordsType targetX, CoordsType targetY,
                          GoalBounding::Direction dir )
        {
            return true;
        }

        virtual std::vector<Point<CoordsType>> find( CoordsType startX, CoordsType startY,
                                                     CoordsType targetX,
                                                     CoordsType targetY,
                                                     uint32_t unitType ) override
        {
            if( !m_map.IsPassable( targetX, targetY, unitType ))
            {
                return { };
            }

            OpenListType openList;
            openList.push( PriorityNode<CoordsType>( startX, startY, 0 ));

            // Only visited flag should be cleared on every search
            std::for_each( m_nodesInfo.begin(), m_nodesInfo.end(), [ ]( auto& info )
            {
                info.SetVisited( false );
            } );

            const auto startPoint = Point<CoordsType>( startX, startY );

            const auto startIdx = coords_to_contiguous_idx( startX, startY, m_map.GetSizeX());
            NodeInfo& startInfo = m_nodesInfo[startIdx];
            startInfo.SetNodeCost( 0 );
            startInfo.SetCameFrom( startPoint.x, startPoint.y );
            startInfo.SetVisited( true );

            while( !openList.empty())
            {
                const auto best = openList.top();
                openList.pop();

                if( best.Equals( targetX, targetY ))
                {
                    break;
                }

                const auto currentIdx = coords_to_contiguous_idx( best.x, best.y, m_map.GetSizeX());
                const auto currentCost = m_nodesInfo[currentIdx].GetNodeCost();

                static std::pair<short, short> coordDeltas[] = {{ -1, 0 },
                                                                { 0,  -1 },
                                                                { 1,  0 },
                                                                { 0,  1 },
                                                                { -1, -1 },
                                                                { -1, 1 },
                                                                { 1,  1 },
                                                                { 1,  -1 },
                };

                static GoalBounding::Direction dirs[] = { GoalBounding::Direction::LEFT,
                                                          GoalBounding::Direction::UP,
                                                          GoalBounding::Direction::RIGHT,
                                                          GoalBounding::Direction::DOWN,
                                                          GoalBounding::Direction::UP_LEFT,
                                                          GoalBounding::Direction::DOWN_LEFT,
                                                          GoalBounding::Direction::DOWN_RIGHT,
                                                          GoalBounding::Direction::UP_RIGHT
                };

                for( int i = 0; i < 8; i++ )
                {
                    const auto& delta = coordDeltas[i];
                    const CoordsType nextX = best.x + delta.first;
                    const CoordsType nextY = best.y + delta.second;
                    if( m_map.IsNodeOnMap( nextX, nextY ))
                    {
                        if( TestGoalBounding( best.x, best.y, targetX, targetY, dirs[i] ))
                        {
                            const auto& node = m_map.GetNode( nextX, nextY );
                            if( node.IsPassable( unitType ))
                            {
                                float newCost = 0;
                                if( i >= 4 )
                                {
                                    if( !m_map.IsPassable( nextX - delta.first, nextY, unitType ) &&
                                        !m_map.IsPassable( nextX, nextY - delta.second, unitType ))
                                    {
                                        continue;
                                    }

                                    newCost = currentCost + node.GetWeight() * std::sqrt( 2 );
                                }
                                else
                                {
                                    newCost = currentCost + node.GetWeight();
                                }


                                const auto nextNodeIdx = coords_to_contiguous_idx( nextX, nextY, m_map.GetSizeX());
                                NodeInfo& nextNodeInfo = m_nodesInfo[nextNodeIdx];

                                if( !nextNodeInfo.IsVisited() || newCost < nextNodeInfo.GetNodeCost())
                                {
                                    nextNodeInfo.SetVisited( true );
                                    nextNodeInfo.SetNodeCost( newCost );
                                    nextNodeInfo.SetCameFrom( best.x, best.y );

                                    float priority =
                                            newCost +
                                            Heuritic::nodes_distance( nextX, nextY, targetX, targetY, startX, startY );

                                    openList.push( PriorityNode<CoordsType>( nextX, nextY, priority ));
                                }
                            }
                        }
                    }
                }
            }

            std::vector<Point<CoordsType>> path;
            auto current = Point<CoordsType>( targetX, targetY );
            path.push_back( current );
            while( !current.Equals( startPoint ))
            {
                const auto& info = m_nodesInfo[coords_to_contiguous_idx( current.x, current.y, m_map.GetSizeX())];
                if( current == info.GetCameFrom())
                {
                    break;
                }

                current = info.GetCameFrom();
                path.push_back( current );
            }

            return path;
        }
    };


    using AStar16 = SimpleAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::StraightDistance
    >;

    using AStar16GoalBounded = SimpleAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::StraightDistance,
            EnableGoalBounding
    >;

    template<typename CoordsType, class MapType, typename Heuritic, typename DbgStatus = DisableDbg, typename GoalBoundingStatus = DisableGoalBounding>
    class JPSAStar : public PathFinder<CoordsType>
    {
        template<typename T>
        friend
        class DisplayedMap;

    private:

        using OpenListType = std::priority_queue<pf::JPSPriorityNode<CoordsType>,
                std::vector<pf::JPSPriorityNode<CoordsType>>, std::greater<pf::JPSPriorityNode<CoordsType>>>;


        class NodeInfo
        {
        public:

            void SetNodeCost( float nodeCost )
            {
                m_nodeCost = nodeCost;
            }

            float GetNodeCost() const
            {
                return m_nodeCost;
            }

            void SetCameFrom( CoordsType x, CoordsType y )
            {
                m_cameFrom.x = x;
                m_cameFrom.y = y;
            }

            Point<CoordsType> GetCameFrom() const
            {
                return m_cameFrom;
            }

        private:
            float m_nodeCost{ -1.f };
            Point<CoordsType> m_cameFrom;
        };

        const MapType& m_map;

        // Search temp variables.
        CoordsType m_startX;
        CoordsType m_startY;
        CoordsType m_targetX;
        CoordsType m_targetY;
        PassabilityType m_unitType;
        OpenListType m_openList;

        std::map<uint32_t, NodeInfo> m_closedNodes;

        PF_FUN_ENABLE_IF( GoalBoundingStatus, EnableGoalBounding, bool )
        TestGoalBounding( CoordsType currentX, CoordsType currentY, CoordsType targetX, CoordsType targetY,
                          GoalBounding::Direction dir )
        {
            return m_map.GetGoalBounds()->IsInBound( currentX, currentY, targetX, targetY, dir );
        }

        PF_FUN_ENABLE_IF( GoalBoundingStatus, DisableGoalBounding, bool )
        TestGoalBounding( CoordsType currentX, CoordsType currentY, CoordsType targetX, CoordsType targetY,
                          GoalBounding::Direction dir )
        {
            return true;
        }

        //Debug

        struct DbgLine
        {
            DbgLine( CoordsType currentX, CoordsType currentY, CoordsType nextX, CoordsType nextY )
                    : currentX( currentX ), currentY( currentY ), nextX( nextX ), nextY( nextY )
            { }

            CoordsType currentX;
            CoordsType currentY;
            CoordsType nextX;
            CoordsType nextY;
        };

        std::vector<DbgLine> m_dbgLines;

        PF_FUN_ENABLE_IF( DbgStatus, EnableDbg, bool )
        AddDbgLine( CoordsType currentX, CoordsType currentY, CoordsType nextX, CoordsType nextY )
        {
            m_dbgLines.push_back( DbgLine( currentX, currentY, nextX, nextY ));
            return true;
        }

        PF_FUN_ENABLE_IF( DbgStatus, DisableDbg, bool )
        AddDbgLine( CoordsType currentX, CoordsType currentY, CoordsType nextX, CoordsType nextY )
        {
            return false;
        }

    public:

        JPSAStar( const MapType& map )
                : m_map( map )
        {
        }

        virtual ~JPSAStar()
        { }

        virtual std::vector<Point<CoordsType>> find( CoordsType startX, CoordsType startY,
                                                     CoordsType targetX,
                                                     CoordsType targetY,
                                                     uint32_t unitType ) override
        {
            m_dbgLines.clear();

            m_startX = startX;
            m_startY = startY;
            m_targetX = targetX;
            m_targetY = targetY;
            m_unitType = unitType;

            if( !m_map.IsPassable( targetX, targetY, unitType ))
            {
                return { };
            }

            m_openList = OpenListType();

            const auto startPoint = Point<CoordsType>( startX, startY );

            const auto startIdx = coords_to_contiguous_idx( startX, startY, m_map.GetSizeX());

            m_closedNodes.clear();

            NodeInfo startInfo;
            startInfo.SetNodeCost( 0 );
            startInfo.SetCameFrom( startPoint.x, startPoint.y );
            m_closedNodes[startIdx] = startInfo;

            horizontalJump( startX, startY, 1, true );
            horizontalJump( startX, startY, -1, true );

            verticalJump( startX, startY, 1, true );
            verticalJump( startX, startY, -1, true );

            diagonalJump( startX, startY, 1, 1 );
            diagonalJump( startX, startY, 1, -1 );
            diagonalJump( startX, startY, -1, 1 );
            diagonalJump( startX, startY, -1, -1 );

            while( !m_openList.empty())
            {
                const auto best = m_openList.top();
                m_openList.pop();

                if( best.Equals( targetX, targetY ))
                {
                    break;
                }

                const auto currentIdx = coords_to_contiguous_idx( best.x, best.y, m_map.GetSizeX());

                horizontalJump( best.x, best.y, best.dx, true );
                verticalJump( best.x, best.y, best.dy, true );

                {
                    CoordsType tempX = best.x - best.dx;

                    if( m_map.IsXOnMap( tempX ) && !m_map.IsPassable( tempX, best.y, m_unitType ))
                    {
                        CoordsType tempY = best.y + best.dy;
                        if( m_map.IsYOnMap( tempY ) && m_map.IsPassable( tempX, tempY, m_unitType ))
                        {
                            if( m_map.IsPassable( best.x, tempY, m_unitType ))
                            {
                                diagonalJump( best.x, best.y, -best.dx, best.dy );
                            }
                        }
                    }
                }

                {
                    CoordsType tempY = best.y - best.dy;

                    if( m_map.IsYOnMap( tempY ) && !m_map.IsPassable( best.x, tempY, m_unitType ))
                    {
                        CoordsType tempX = best.x + best.dx;
                        if( m_map.IsXOnMap( tempX ) && m_map.IsPassable( tempX, tempY, m_unitType ))
                        {
                            if( m_map.IsPassable( tempX, best.y, m_unitType ))
                            {
                                diagonalJump( best.x, best.y, best.dx, -best.dy );
                            }
                        }
                    }
                }

                diagonalJump( best.x, best.y, best.dx, best.dy );
            }

            std::vector<Point<CoordsType>> path;
            auto current = Point<CoordsType>( targetX, targetY );
            path.push_back( current );
            while( !current.Equals( startPoint ))
            {
                const auto& info = m_closedNodes[coords_to_contiguous_idx( current.x, current.y, m_map.GetSizeX())];

                if( current == info.GetCameFrom())
                {
                    break;
                }

                current = info.GetCameFrom();
                path.push_back( current );
            }

            return path;
        }

    protected:

        inline
        void checkAndAddJumpPoint( CoordsType currentX, CoordsType currentY, CoordsType nextX, CoordsType nextY,
                                   short dx, short dy )
        {
            const auto jumpNodeIdx = coords_to_contiguous_idx( nextX, nextY, m_map.GetSizeX());
            NodeInfo& jumpNodeInfo = m_closedNodes[jumpNodeIdx];

            static const auto MIN_COST_DELTA = 0.01f;

            const auto prevNodeIdx = coords_to_contiguous_idx( currentX, currentY, m_map.GetSizeX());
            NodeInfo& prevNodeInfo = m_closedNodes[prevNodeIdx];
            auto cost = prevNodeInfo.GetNodeCost() +
                        StraightDistance::nodes_distance<CoordsType>( currentX, currentY, nextX, nextY, 0, 0 );

            if( jumpNodeInfo.GetNodeCost() < 0 || ( cost + MIN_COST_DELTA ) < jumpNodeInfo.GetNodeCost())
            {
                jumpNodeInfo.SetNodeCost( cost );
                jumpNodeInfo.SetCameFrom( currentX, currentY );

                uint32_t priority =
                        cost +
                        Heuritic::nodes_distance( nextX, nextY, m_targetX, m_targetY,
                                                  m_startX, m_startY );

                m_openList.push( JPSPriorityNode<CoordsType>( nextX, nextY, priority, dx, dy ));
            }
        }

        bool diagonalJump( CoordsType currentX, CoordsType currentY, short dx, short dy )
        {
            CoordsType nextX = currentX + dx;
            CoordsType nextY = currentY + dy;

            bool isJumpPoint = false;

            while( m_map.IsNodeOnMap( nextX, nextY ))
            {
                AddDbgLine( nextX - dx, nextY - dy, nextX, nextY );

                if( nextX == m_targetX && nextY == m_targetY )
                {
                    checkAndAddJumpPoint( currentX, currentY, nextX, nextY, 0, 0 );
                    return true;
                }

                short neighboursBlocked = 0;

                if( m_map.IsPassable( nextX, nextY, m_unitType ))
                {
                    CoordsType tempY = nextY + dy;

                    if( m_map.IsYOnMap( tempY ) && m_map.IsPassable( nextX, tempY, m_unitType ))
                    {
                        CoordsType tempX = nextX - dx;
                        if( !m_map.IsPassable( tempX, nextY, m_unitType ) &&
                            m_map.IsPassable( tempX, tempY, m_unitType ))
                        {
                            // We found forced neighbour
                            checkAndAddJumpPoint( currentX, currentY, nextX, nextY, dx, dy );
                            // Even if we didn't add forced neighbour we found it, so return true
                            //isJumpPoint = true;
                            return true;
                        }
                    }
                    else
                    {
                        neighboursBlocked++;
                    }

                    CoordsType tempX = nextX + dx;

                    if( m_map.IsXOnMap( tempX ) && m_map.IsPassable( tempX, nextY, m_unitType ))
                    {
                        CoordsType tempY = nextY - dy;
                        if( !m_map.IsPassable( nextX, tempY, m_unitType ) &&
                            m_map.IsPassable( tempX, tempY, m_unitType ))
                        {
                            // We found forced neighbour
                            checkAndAddJumpPoint( currentX, currentY, nextX, nextY, dx, dy );
                            //isJumpPoint = true;
                            return true;
                        }
                    }
                    else
                    {
                        neighboursBlocked++;
                    }

                    if( neighboursBlocked == 2 )
                    {
                        //return isJumpPoint;
                        return true;
                    }

                    // No forced neighbour found - go in horizontal and vertical directions

                    if( horizontalJump( nextX, nextY, dx, false ) ||
                        verticalJump( nextX, nextY, dy, false ))
                    {
                        checkAndAddJumpPoint( currentX, currentY, nextX, nextY, dx, dy );
//                        isJumpPoint = true;
                        return true;
                    }

//                    if( isJumpPoint )
//                    {
//                        return true;
//                    }

                    nextX += dx;
                    nextY += dy;
                }
                else
                {
                    return false;   // No forced neighbour found.
                }
            }

            return false;
        }


        bool horizontalJump( CoordsType currentX, CoordsType currentY, short dx, bool addJumpPoint )
        {
            CoordsType nextX = currentX + dx;

            bool result = false;

            if( !m_map.IsPassableXSafe( nextX, currentY, m_unitType ))
            {
                return false;
            }

            if( nextX == m_targetX && currentY == m_targetY )
            {
                if( addJumpPoint )
                {
                    checkAndAddJumpPoint( currentX, currentY, nextX, currentY, 0, 0 );
                }
                return true;
            }

            if( !TestGoalBounding( currentX, currentY, m_targetX, m_targetY,
                                   dx > 0 ? GoalBounding::Direction::RIGHT : GoalBounding::Direction::LEFT ))
            {
                return false;
            }

            uint32_t testForcedPrev = ~( m_map.IsPassableYSafe( nextX, currentY + 1, m_unitType ) |
                                         (( m_map.IsPassableYSafe( nextX, currentY - 1, m_unitType )) << 1 ));

            nextX += dx;
            while( nextX < m_map.GetSizeX())
            {
                if( !m_map.IsPassableXSafe( nextX, currentY, m_unitType ))
                {
                    return false;
                }

                AddDbgLine( nextX - 2 * dx, currentY, nextX - dx, currentY );

                uint32_t testForcedCurrent = ( m_map.IsPassableYSafe( nextX, currentY + 1, m_unitType ) |
                                               (( m_map.IsPassableYSafe( nextX, currentY - 1, m_unitType )) << 1 ));


                auto forcedPos = testForcedPrev & testForcedCurrent;    // 1 or 2, where 1 - down and 2 up
                if( forcedPos || ( nextX == m_targetX && currentY == m_targetY ))
                {
                    if( addJumpPoint )
                    {
                        short dy = forcedPos == 1 ? 1 : -1;
                        checkAndAddJumpPoint( currentX, currentY, nextX - dx, currentY, dx, dy );
                    }
                    return true;
                }

//                if( !TestGoalBounding(nextX, currentY, m_targetX, m_targetY,
//                                      dx > 0 ? GoalBounding::Direction::RIGHT : GoalBounding::Direction::LEFT))
//                {
//                    return false;
//                }

                testForcedPrev = ~testForcedCurrent;
                nextX += dx;
            }

            return false;
        }

        bool verticalJump( CoordsType currentX, CoordsType currentY, short dy, bool addJumpPoint )
        {
            CoordsType nextY = currentY + dy;

            if( !m_map.IsPassableYSafe( currentX, nextY, m_unitType ))
            {
                return false;
            }

            if( currentX == m_targetX && nextY == m_targetY )
            {
                if( addJumpPoint )
                {
                    checkAndAddJumpPoint( currentX, currentY, currentX, nextY, 0, 0 );
                }
                return true;
            }

            if( !TestGoalBounding( currentX, currentY, m_targetX, m_targetY,
                                   dy > 0 ? GoalBounding::Direction::DOWN : GoalBounding::Direction::UP ))
            {
                return false;
            }

            // Preventing unnecessary comparisons
            uint32_t testForcedPrev = ~( m_map.IsPassableXSafe( currentX + 1, nextY, m_unitType ) |
                                         (( m_map.IsPassableXSafe( currentX - 1, nextY, m_unitType )) << 1 ));

            nextY += dy;
            while( nextY < m_map.GetSizeY())
            {
                if( !m_map.IsPassableYSafe( currentX, nextY, m_unitType ))
                {
                    return false;
                }

                AddDbgLine( currentX, nextY - 2 * dy, currentX, nextY - dy );

                uint32_t testForcedCurrent = ( m_map.IsPassableXSafe( currentX + 1, nextY, m_unitType ) |
                                               (( m_map.IsPassableXSafe( currentX - 1, nextY, m_unitType )) << 1 ));

                auto forcedPos = testForcedPrev & testForcedCurrent;    // 1 or 2, where 1 - down and 2 up
                if( forcedPos || ( currentX == m_targetX && nextY == m_targetY ))
                {
                    if( addJumpPoint )
                    {
                        short dx = forcedPos == 1 ? 1 : -1;
                        checkAndAddJumpPoint( currentX, currentY, currentX, nextY - dy, dx, dy );
                    }
                    return true;
                }

//                if( !TestGoalBounding(currentX, nextY, m_targetX, m_targetY,
//                                      dy > 0 ? GoalBounding::Direction::DOWN : GoalBounding::Direction::UP))
//                {
//                    return false;
//                }

                testForcedPrev = ~testForcedCurrent;
                nextY += dy;
            }

            return false;
        }
    };

    using JPSAStar16 = JPSAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::ManhattanDistance,
            DisableDbg,
            DisableGoalBounding
    >;

    using JPSAStar16GBound = JPSAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::ManhattanDistance,
            DisableDbg,
            EnableGoalBounding
    >;

    using JPSAStar16Dbg = JPSAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::ManhattanDistance,
            EnableDbg,
            DisableGoalBounding
    >;

    using JPSAStar16GBoundDbg = JPSAStar<
            uint16_t,
            pf::SimpleMap<uint16_t>,
            pf::ManhattanDistance,
            EnableDbg,
            EnableGoalBounding
    >;
}
