#pragma once

#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <set>
#include "boost/filesystem.hpp"
#include <boost/functional/hash.hpp>
#include "boost/exception/diagnostic_information.hpp"
#include "threadpool11/threadpool11.hpp"

#include <pathpf/Map.h>
#include <pathpf/test/MapLoaders.h>
#include <pathpf/time/MeasureUtils.h>
#include <pathpf/AStar.h>

enum class AlgType
{
    A_STAR,
    A_STAR_GOAL,
    JPS,
    JPS_GOAL,
    JPS_REF
};

struct ScenarioEntry
{
    uint32_t m_bucket;
    std::string m_mapPath;
    uint16_t m_mapWidth;
    uint16_t m_mapHeight;
    uint16_t m_startX;
    uint16_t m_startY;
    uint16_t m_goalX;
    uint16_t m_goalY;
    float m_expectedPathCost;

    AlgType m_algType;

    int m_warmUpCount;
    int m_callsCount;

    std::size_t m_hash;
};

struct FinderCacheKey
{
    std::string m_mapName;
    AlgType m_algType;
    std::thread::id m_threadId;

    bool operator==( const FinderCacheKey& rhs ) const
    {
        return m_mapName == rhs.m_mapName && m_algType == rhs.m_algType && m_threadId == rhs.m_threadId;
    }
};


namespace std
{
    template<>
    struct hash<ScenarioEntry>
    {
        std::size_t operator()( const ScenarioEntry& c ) const
        {
            std::size_t result = 0;
            boost::hash_combine( result, c.m_bucket );
            boost::hash_combine( result, c.m_mapPath );
            boost::hash_combine( result, c.m_mapWidth );
            boost::hash_combine( result, c.m_mapHeight );
            boost::hash_combine( result, c.m_startX );
            boost::hash_combine( result, c.m_startY );
            boost::hash_combine( result, c.m_goalX );
            boost::hash_combine( result, c.m_goalY );
            boost::hash_combine( result, c.m_expectedPathCost );
            boost::hash_combine( result, c.m_algType );
            return result;
        }
    };

    template<>
    struct hash<FinderCacheKey>
    {
        std::size_t operator()( const FinderCacheKey& c ) const
        {
            std::size_t result = 0;
            boost::hash_combine( result, c.m_mapName );
            boost::hash_combine( result, c.m_algType );
            boost::hash_combine( result, std::hash<std::thread::id>()( c.m_threadId ));
            return result;
        }
    };
}


class ScenarioExecuter
{
private:

    struct Result
    {
        std::string m_mapName;
        uint16_t m_mapWidth;
        uint16_t m_mapHeight;
        float m_expectedPathCost;
        float m_resultPathCost;
        long m_funcTime;
        long m_totalTime;
        int m_callsCount;
        uint16_t m_startX;
        uint16_t m_startY;
        uint16_t m_goalX;
        uint16_t m_goalY;
        AlgType m_algType;

        std::size_t m_scenarioHash;
    };

public:

    void LoadScenarios( std::string folderPath, std::string outPath, std::vector<AlgType> algsToTest )
    {
        std::cout << "Loading scenarios" << std::endl;

        std::hash<ScenarioEntry> hash;

        using namespace boost::filesystem;
        recursive_directory_iterator end;
        for( recursive_directory_iterator it( folderPath ); it != end; ++it )
        {
            std::cout << "Loading " << *it << std::endl;

            ifstream is( *it );

            std::string tmp;
            float version;

            is >> tmp >> version;

            while( true )
            {
                ScenarioEntry entry;
                is >> entry.m_bucket;
                is >> entry.m_mapPath;
                is >> entry.m_mapWidth;
                is >> entry.m_mapHeight;
                is >> entry.m_startX;
                is >> entry.m_startY;
                is >> entry.m_goalX;
                is >> entry.m_goalY;
                is >> entry.m_expectedPathCost;

                if( is.eof())
                {
                    break;
                }

                for( auto& alg : algsToTest )
                {
                    auto entryCopy = entry;
                    entryCopy.m_algType = alg;
                    entryCopy.m_hash = hash( entryCopy );
                    m_scenarios.push_back( entryCopy );
                }
            }
        }

        std::string performedScenariosFilePath = outPath + "progress.stats";

        ifstream is( performedScenariosFilePath );
        if( is )
        {
            while( true )
            {
                std::size_t h;
                is >> h;

                if( is.eof())
                {
                    break;
                }

                m_processedScenarios.emplace( h );
            }

        }

        std::cout << "Done loading scenarios" << std::endl;
    }

    void TestScenarios( std::string outPath, bool calcGoalBounds, uint32_t maxMapArea )
    {
        std::map<std::string, std::unique_ptr<pf::SimpleMap<uint16_t>>> maps;

        std::cout << "Preloading maps" << std::endl;

        // Preload all maps
        for( auto& scenario : m_scenarios )
        {
            if( maps.find( scenario.m_mapPath ) == maps.end())
            {
//                std::cout << "Loading " << scenario.m_mapPath << std::endl;
                auto mapPtr = loaders::load_map_ai_lab_format<uint16_t>( scenario.m_mapPath );

                if( mapPtr != nullptr )
                {
                    std::unique_ptr<pf::SimpleMap<uint16_t>> map( mapPtr );
                    maps.insert(
                            std::make_pair( scenario.m_mapPath, std::move( map )));
                }
                else
                {
//                    std::cout << "Can't load " << scenario.m_mapPath << std::endl;
                }
            }

            CalcScenarioParameters( scenario );
        }

        std::cout << "Preloading maps done!" << std::endl;

        // Remove processed scenarios and ones without map
        m_scenarios.erase( std::remove_if( m_scenarios.begin(), m_scenarios.end(),
                                           [ this, &maps ]( const ScenarioEntry& scenarioEntry )
                                           {
                                               if( m_processedScenarios.find( scenarioEntry.m_hash ) !=
                                                   m_processedScenarios.end())
                                               {
                                                   return true;
                                               }

                                               auto mapIt = maps.find( scenarioEntry.m_mapPath );

                                               if( mapIt == maps.end())
                                               {
                                                   return true;
                                               }

                                               return false;

                                           } ), m_scenarios.end());

        // Sort scenarios by map size - process small first
        std::sort( m_scenarios.begin(), m_scenarios.end(),
                   [ ]( const ScenarioEntry& s1, const ScenarioEntry& s2 ) -> bool
                   {
                       auto s1Size = s1.m_mapWidth * s1.m_mapHeight;
                       auto s2Size = s2.m_mapWidth * s2.m_mapHeight;

                       if( s1Size < s2Size )
                       {
                           return true;
                       }
                       else if( s1Size > s2Size )
                       {
                           return false;
                       }
                       else
                       {
                           return s1.m_mapPath < s2.m_mapPath;
                       }
                   } );


        auto scenarioProcessor = [ &maps, this ]( const ScenarioEntry& scenario, std::mutex& resultMutex,
                                                  std::mutex& mapMutex )
        {
            try
            {
                auto it = maps.find( scenario.m_mapPath );
                auto map = it->second.get();

                if(( scenario.m_algType == AlgType::A_STAR_GOAL ||
                     scenario.m_algType == AlgType::JPS_GOAL ) &&
                   map->GetGoalBounds() == nullptr )
                {
                    return;
                }

                MeasureUtils::Settings measureSettings( scenario.m_callsCount,
                                                        scenario.m_warmUpCount );

                pf::PathFinder<uint16_t>* pathFinder{ nullptr };

                {
                    std::lock_guard<std::mutex> lck( mapMutex );
                    pathFinder = GetPathFinder( scenario.m_algType, *map,
                                                std::this_thread::get_id());
                }

                auto result = MeasureUtils::measureMethodEvalTime<std::chrono::nanoseconds>(
                        measureSettings,
                        &pf::PathFinder<uint16_t>::find,
                        pathFinder,
                        scenario.m_startX, scenario.m_startY,
                        scenario.m_goalX, scenario.m_goalY, 1u );

                Result res;
                res.m_mapName = map->GetName();
                res.m_mapWidth = map->GetSizeX();
                res.m_mapHeight = map->GetSizeY();
                res.m_expectedPathCost = scenario.m_expectedPathCost;
                res.m_funcTime = result.time;
                res.m_totalTime = result.totalTime;
                res.m_callsCount = scenario.m_callsCount;
                res.m_algType = scenario.m_algType;
                res.m_startX = scenario.m_startX;
                res.m_startY = scenario.m_startY;
                res.m_goalX = scenario.m_goalX;
                res.m_goalY = scenario.m_goalY;
                res.m_scenarioHash = scenario.m_hash;

                std::vector<pf::Point<uint16_t>>& path = result.result;
                res.m_resultPathCost = 0;
                for( int i = 1; i < path.size(); i++ )
                {
                    int32_t dx = ( (int32_t)path[i - 1].x - (int32_t)path[i].x );
                    int32_t dy = ( (int32_t)path[i - 1].y - (int32_t)path[i].y );
                    res.m_resultPathCost += std::sqrt( dx * dx + dy * dy );
                }

                std::lock_guard<std::mutex> lck( resultMutex );
                m_results.push_back( res );
            }
            catch( ... )
            {
                std::clog << boost::current_exception_diagnostic_information() << std::endl;
            }
        };

        m_results.clear();

        std::ofstream os( outPath + "results.csv", std::ofstream::out | std::ofstream::app );
        std::ofstream processedScenariosOs( outPath + "progress.stats", std::ofstream::out | std::ofstream::app );

        if( !os )
        {
            std::cout << "Failed to open resutls" << std::endl;
            return;
        }

        os <<
        "Map, Width, Height, Alg, Calls, Time, Total time, Expected Cost, Result Cost, Start X, Start Y, Goal X, Goal Y" <<
        std::endl;

        threadpool11::Pool pool( std::thread::hardware_concurrency() / 2 );
        std::mutex resultMutex;
        std::mutex mapMutex;

        for( size_t i = 0; i < m_scenarios.size(); i++ )
        {
            const auto& mapPath = m_scenarios[i].m_mapPath;
            auto mapIt = maps.find( m_scenarios[i].m_mapPath );
            auto map = mapIt->second.get();

            if( calcGoalBounds )
            {
                if( map->GetSizeX() * map->GetSizeY() <= maxMapArea )
                {
                    std::cout << "Calculating goal bounds for " << mapIt->first << std::endl;
                    map->CalculateGoalBounds( "gbounds\\" );
                    std::cout << "End Calculating goal bounds for " << mapIt->first << std::endl;
                }
                else
                {
                    std::cout << "Won't calc goal bounds for " << mapIt->first << std::endl;
                }
            }
            else
            {
                std::cout << "Loading goal bounds" << std::endl;
                for( const auto& mapIt : maps )
                {
                    auto map = mapIt.second.get();
                    map->LoadGoalBounds( "gbounds\\" );
                }

                std::cout << "Done loading goal bounds!" << std::endl;
            }

            size_t last = i;
            while( mapPath == m_scenarios[last].m_mapPath )
            {
                pool.postWork<void>( std::bind( scenarioProcessor, m_scenarios[last], std::ref( resultMutex ),
                                                std::ref( mapMutex )));
                last++;
            }

            pool.waitAll();

            for( const auto& res : m_results )
            {
                os << res.m_mapName << "," << res.m_mapWidth << "," << res.m_mapHeight
                << "," << GetAlgName( res.m_algType ) << "," << res.m_callsCount << "," << res.m_funcTime << "," <<
                res.m_totalTime << "," << res.m_expectedPathCost << "," << res.m_resultPathCost << ","
                << res.m_startX << "," << res.m_startY << "," << res.m_goalX << "," <<
                res.m_goalY << std::endl;

                processedScenariosOs << res.m_scenarioHash << std::endl;
            }

            m_results.clear();

            std::cout << "[" << last << "/" << m_scenarios.size() << "]" << std::endl;

            i = last - 1;
        }

        std::cout << "Done!" << std::endl;
    }

private:

    void CalcScenarioParameters( ScenarioEntry& scenario )
    {
        if( scenario.m_algType == AlgType::A_STAR )
        {
            if( scenario.m_expectedPathCost != 0 )
            {
                scenario.m_callsCount = static_cast<int>(3000 / scenario.m_expectedPathCost);
            }
            else
            {
                scenario.m_callsCount = 100;
            }
        }
        else if( scenario.m_algType == AlgType::A_STAR_GOAL )
        {
            if( scenario.m_expectedPathCost != 0 )
            {
                scenario.m_callsCount = static_cast<int>(10000 / scenario.m_expectedPathCost);
            }
            else
            {
                scenario.m_callsCount = 400;
            }
        }
        else if( scenario.m_algType == AlgType::JPS )
        {
            if( scenario.m_expectedPathCost != 0 )
            {
                scenario.m_callsCount = static_cast<int>(50000 / scenario.m_expectedPathCost);
            }
            else
            {
                scenario.m_callsCount = 1500;
            }
        }
        else if( scenario.m_algType == AlgType::JPS_GOAL )
        {
            if( scenario.m_expectedPathCost != 0 )
            {
                scenario.m_callsCount = static_cast<int>(150000 / scenario.m_expectedPathCost);
            }
            else
            {
                scenario.m_callsCount = 4000;
            }
        }

        scenario.m_warmUpCount = std::max( scenario.m_callsCount / 10, 1 );
    }

    pf::PathFinder<uint16_t>* GetPathFinder( AlgType algType, pf::SimpleMap<uint16_t>& map, std::thread::id threadId )
    {
        FinderCacheKey key;
        key.m_mapName = map.GetName();
        key.m_algType = algType;
        key.m_threadId = threadId;

        auto it = m_findersCache.find( key );

        if( it != m_findersCache.end())
        {
            return it->second.get();
        }
        else
        {
            switch( algType )
            {
                case AlgType::A_STAR:
                    m_findersCache[key] = std::make_unique<pf::AStar16>( map );
                    break;
                case AlgType::A_STAR_GOAL:
                    m_findersCache[key] = std::make_unique<pf::AStar16GoalBounded>( map );
                    break;
                case AlgType::JPS:
                    m_findersCache[key] = std::make_unique<pf::JPSAStar16>( map );
                    break;
                case AlgType::JPS_GOAL:
                    m_findersCache[key] = std::make_unique<pf::JPSAStar16GBound>( map );
                    break;
                default:
                    return nullptr;
            }

            return m_findersCache.find( key )->second.get();
        }
    }

    static std::string GetAlgName( AlgType algType )
    {
        switch( algType )
        {
            case AlgType::A_STAR:
                return "AStar";
            case AlgType::A_STAR_GOAL:
                return "AStar+GB";
            case AlgType::JPS:
                return "JPS";
            case AlgType::JPS_GOAL:
                return "JPS+GB";
        }

        return "Undefined";
    }

private:

    std::vector<ScenarioEntry> m_scenarios;
    std::set<std::size_t> m_processedScenarios;
    std::vector<Result> m_results;

    std::unordered_map<FinderCacheKey, std::unique_ptr<pf::PathFinder<uint16_t>>> m_findersCache;
};