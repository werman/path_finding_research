#include <stdio.h>
#include <iostream>
#include <pathpf/GoalBounding.h>

#include "pathpf/test/MapLoaders.h"
#include "pathpf/time/MeasureUtils.h"
#include "pathpf/AStar.h"

#include "Scenario.h"

int main( int argc, char* argv[] )
{
    //auto map = loaders::load_map_ai_lab_format<int16_t>( "room950.map" );

//    auto map = loaders::load_map_ai_lab_format<uint16_t>( "brc504d.map" );

//    pf::JPSAStar searcher( map );
//
//    //searcher.find(2u, 0u, 9u, 0u, 1u);
//
//    MeasureUtils::Settings measureSettings( 80, 10 );
//    auto duration = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings, &pf::JPSAStar::find, &searcher, 523u, 928u, 79u, 66u, 1u);
//
//    std::cout << duration.time << std::endl;

    //pf_func_s( map, 6u, 1u, 5u, 5u, 1u);

//    pf::GoalBounding gBounding;
//    gBounding.Process( map, 1u );

    ScenarioExecuter executer;
    executer.LoadScenarios( "scen\\", "out\\", { AlgType::A_STAR, AlgType::JPS, AlgType::A_STAR_GOAL, AlgType::JPS_GOAL } );
    //executer.TestScenarios( "out\\", true, 512 * 512 );
    executer.TestScenarios( "out\\", true, 512 * 512 );
    return 0;
}
