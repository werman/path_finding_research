#include <iostream>
#include <random>

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>

#include <SFGUI/RendererViewport.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/OpenGL.hpp>

#include <boost/algorithm/clamp.hpp>
#include <boost/format.hpp>

#include "pathpf/test/MapLoaders.h"
#include "pathpf/time/MeasureUtils.h"
#include "pathpf/AStar.h"

#define JPS_DISABLE_GREEDY

#include "pathpf/other/JPS.h"
#include "tinyfiledialogs/tinyfiledialogs.h"

using namespace std;

static float k_quadSize = 2.f;

void fill_quad( sf::Vertex* quad, int x, int y, sf::Color color, float quadWidth )
{
    float xCoord = x * quadWidth;
    float yCoord = y * quadWidth;
    quad[0].position = sf::Vector2f( xCoord, yCoord );
    quad[1].position = sf::Vector2f( xCoord + quadWidth, yCoord );
    quad[2].position = sf::Vector2f( xCoord + quadWidth, yCoord + quadWidth );
    quad[3].position = sf::Vector2f( xCoord, yCoord + quadWidth );

    quad[0].color = color;
    quad[1].color = color;
    quad[2].color = color;
    quad[3].color = color;
}

template<typename CoordsType>
class DisplayedMap : public sf::Drawable
{
public:

    DisplayedMap( const pf::SimpleMap<CoordsType>& map )
            : m_map( map )
    {
        m_dbgLines.setPrimitiveType( sf::PrimitiveType::Lines );
    }

    void Init()
    {
        m_mapPixels.reset( new sf::Uint8[m_map.GetSizeX() * m_map.GetSizeY() * 4 * 4] );
        m_pathPixels.reset( new sf::Uint8[m_map.GetSizeX() * m_map.GetSizeY() * 4] );
        m_ffPixels.reset( new sf::Uint8[m_map.GetSizeX() * m_map.GetSizeY() * 4] );

        m_mapTexture.create( m_map.GetSizeX() * 2, m_map.GetSizeY() * 2 );
        m_pathTexture.create( m_map.GetSizeX(), m_map.GetSizeY());
        m_ffTexture.create( m_map.GetSizeX(), m_map.GetSizeY());

        m_mapTexture.setSmooth( true );

        m_mapSprite.setTexture( m_mapTexture );
        m_pathSprite.setTexture( m_pathTexture );
        m_pathSprite.setScale( 2.f, 2.f );
        m_ffSprite.setTexture( m_ffTexture );
        m_ffSprite.setScale( 2.f, 2.f );

        for( auto i = 0; i < m_map.GetSizeX(); i++ )
        {
            for( auto j = 0; j < m_map.GetSizeY(); j++ )
            {
                auto& node = m_map.GetNode( i, j );

                auto color = sf::Color::Red;
                switch( node.GetType())
                {
                    case TerrainType::UNPASSABLE:
                        color = sf::Color::Red;
                        break;
                    case TerrainType::GROUND:
                        color = sf::Color( 150, 75, 0 ); // brown
                        break;
                    case TerrainType::FOREST:
                        color = sf::Color( 79, 121, 66 ); //dark green
                        break;
                    case TerrainType::WATER:
                        color = sf::Color( 0, 102, 204 ); //blue
                        break;
                    case TerrainType::SWAWMP:
                        color = sf::Color( 102, 0, 204 ); //dark green
                        break;
                }

                sf::Uint8 weightColor = node.GetWeight();
//                SetPixel( m_mapPixels.get(), m_map.GetSizeX() * 2, i * 2, j * 2,
//                          sf::Color( weightColor, weightColor, weightColor ));

                SetPixel( m_mapPixels.get(), m_map.GetSizeX() * 2, i * 2, j * 2, color );
                SetPixel( m_mapPixels.get(), m_map.GetSizeX() * 2, i * 2 + 1, j * 2, color );
                SetPixel( m_mapPixels.get(), m_map.GetSizeX() * 2, i * 2, j * 2 + 1, color );
                SetPixel( m_mapPixels.get(), m_map.GetSizeX() * 2, i * 2 + 1, j * 2 + 1, color );
            }
        }

        m_mapTexture.update( m_mapPixels.get());
    }

    void SetFFMap( const pf::GoalBounding::FFMap* ffMap )
    {
        m_ffMap = ffMap;
    }

    void SetDrawFFMap( bool draw )
    {
        m_drawFFMap = draw;
    }

    void ClearOverlay()
    {
        m_dbgLines.clear();
        for( auto i = 0; i < m_map.GetSizeX(); i++ )
        {
            for( auto j = 0; j < m_map.GetSizeY(); j++ )
            {
                int visitedIdx = i + j * m_map.GetSizeX();
                int idx = visitedIdx * 4;
                m_pathPixels.get()[idx + 3] = 0;
            }
        }
    }

    void AddPath( const std::vector<pf::Point<CoordsType>>& path )
    {
        auto visitedColor = sf::Color( 255, 153, 51, 100 );


        auto color = sf::Color::Yellow;
        for( auto point : path )
        {
            SetPixel( m_pathPixels.get(), m_map.GetSizeX(), point.x, point.y, color );
        }
    }

    void UpdateFFMap()
    {
        std::random_device rd;
        std::mt19937 mt( rd());
        std::uniform_int_distribution<int> rng( 0, 255 );

        sf::Color dirColors[9];

        for( int i = 0; i < 8; i++ )
        {
            dirColors[i] = sf::Color( rng( mt ), rng( mt ), rng( mt ), 255 );
        }

        dirColors[8] = sf::Color::Black;

        for( auto i = 0; i < m_map.GetSizeX(); i++ )
        {
            for( auto j = 0; j < m_map.GetSizeY(); j++ )
            {
                auto node = m_ffMap->GetNode( i, j );
                auto color = dirColors[static_cast<int>(node.originalDir)];

                SetPixel( m_ffPixels.get(), m_map.GetSizeX(), i, j, color );
            }
        }

        m_ffTexture.update( m_ffPixels.get());
    }

    void DrawBoundingBoxes( CoordsType x, CoordsType y )
    {
        m_bboxPointX = x;
        m_bboxPointY = y;
    }

    template<typename ... Args>
    void AddDbgInfo( pf::JPSAStar<Args...>& searcher )
    {
        auto visitedColor = sf::Color( 0, 0, 255, 100 );

        for( const auto& closed : searcher.m_closedNodes )
        {
            auto coords = pf::contiguous_idx_to_coords( closed.first, m_map.GetSizeX());
            SetPixel( m_pathPixels.get(), m_map.GetSizeX(), std::get<0>( coords ), std::get<1>( coords ),
                      visitedColor );
        }

        m_dbgLines.clear();

        sf::Vector2f delta( k_quadSize / 2, k_quadSize / 2 );

        for( const auto& line: searcher.m_dbgLines )
        {
            m_dbgLines.append( sf::Vertex( sf::Vector2f( line.currentX, line.currentY ) * k_quadSize + delta,
                                           sf::Color::Green ));
            m_dbgLines.append( sf::Vertex( sf::Vector2f( line.nextX, line.nextY ) * k_quadSize + delta,
                                           sf::Color::Blue ));
        }
    }

    template<typename ... Args>
    void AddDbgInfo( pf::SimpleAStar<Args...>& searcher )
    {
        auto visitedColor = sf::Color( 0, 0, 255, 100 );
        for( auto i = 0; i < m_map.GetSizeX(); i++ )
        {
            for( auto j = 0; j < m_map.GetSizeY(); j++ )
            {
                int visitedIdx = i + j * m_map.GetSizeX();

                //if( showVisited && pf::g_debugVisited[visitedIdx] )
                if( searcher.m_nodesInfo[visitedIdx].IsVisited())
                {
                    SetPixel( m_pathPixels.get(), m_map.GetSizeX(), i, j, visitedColor );
                }
            }
        }
    }

    template<typename ... Args>
    void AddDbgInfo( JPS::Searcher<Args...>& searcher )
    {
        m_dbgLines.clear();

        sf::Vector2f delta( k_quadSize / 2, k_quadSize / 2 );

        for( const auto& line: searcher.m_dbgLines )
        {
            m_dbgLines.append( sf::Vertex( sf::Vector2f( line.currentX, line.currentY ) * k_quadSize + delta,
                                           sf::Color::Green ));
            m_dbgLines.append( sf::Vertex( sf::Vector2f( line.nextX, line.nextY ) * k_quadSize + delta,
                                           sf::Color::Blue ));
        }
    }

    void Draw()
    {
        m_pathTexture.update( m_pathPixels.get());
    }

    virtual void draw( sf::RenderTarget& target, sf::RenderStates states ) const override
    {
        target.draw( m_mapSprite, states );
        target.draw( m_pathSprite, states );

        if( m_drawFFMap )
        {
            target.draw( m_ffSprite, states );
        }

        target.draw( m_dbgLines, states );

        auto goalBounds = m_map.GetGoalBounds();
        if( goalBounds != nullptr )
        {
            auto nodeBBInfo = goalBounds->GetBBInfo( m_bboxPointX, m_bboxPointY );
            for( int i = 0; i < 8; i++ )
            {
                auto bb = nodeBBInfo.boundingBoxes[i];

                sf::Vertex line[] =
                        {
                                sf::Vertex( sf::Vector2f( bb.minX, bb.minY ) * k_quadSize ),
                                sf::Vertex( sf::Vector2f( bb.maxX, bb.minY ) * k_quadSize ),
                                sf::Vertex( sf::Vector2f( bb.maxX, bb.maxY ) * k_quadSize ),
                                sf::Vertex( sf::Vector2f( bb.minX, bb.maxY ) * k_quadSize ),
                                sf::Vertex( sf::Vector2f( bb.minX, bb.minY ) * k_quadSize ),
                        };

                target.draw( line, 5, sf::PrimitiveType::LinesStrip );
            }
        }
    }

private:

    void SetPixel( sf::Uint8* pixels, int sizeX, int x, int y, const sf::Color& color )
    {
        int idx = ( x + y * sizeX ) * 4;
        pixels[idx] = color.r;
        pixels[idx + 1] = color.g;
        pixels[idx + 2] = color.b;
        pixels[idx + 3] = color.a;
    }

private:

    const pf::SimpleMap<CoordsType>& m_map;

    const pf::GoalBounding::FFMap* m_ffMap{ nullptr };
    bool m_drawFFMap{ false };

    std::unique_ptr<sf::Uint8> m_mapPixels;
    std::unique_ptr<sf::Uint8> m_pathPixels;
    std::unique_ptr<sf::Uint8> m_ffPixels;

    sf::Texture m_mapTexture;
    sf::Texture m_pathTexture;
    sf::Texture m_ffTexture;

    sf::Sprite m_mapSprite;
    sf::Sprite m_pathSprite;
    sf::Sprite m_ffSprite;

    sf::VertexArray m_dbgLines;

    CoordsType m_bboxPointX{ 0 };
    CoordsType m_bboxPointY{ 0 };
};

template<typename CoordsType>
sf::Vector2i get_tile_under_mouse( const sf::RenderWindow& renderWindow, const sfg::Canvas::Ptr& canvas,
                                   const pf::SimpleMap<CoordsType>& map )
{
    sf::Vector2i mouseCoords = sf::Mouse::getPosition( renderWindow );

//    mouseCoords -= sf::Vector2i( 11, 35 );  //TODO actually get them and not hardcode!!!

    sf::Vector2f normalized;
    auto viewSize = canvas->GetRequisition();
    normalized.x = -1.f + 2.f * mouseCoords.x / viewSize.x;
    normalized.y = 1.f - 2.f * mouseCoords.y / viewSize.y;

    auto coords = canvas->GetView().getInverseTransform().transformPoint( normalized );

    coords /= 2.f;

    int x = static_cast<int>( std::floor( boost::algorithm::clamp( coords.x, 0, map.GetSizeX() - 1 )));
    int y = static_cast<int>( std::floor( boost::algorithm::clamp( coords.y, 0, map.GetSizeY() - 1 )));

    return { x, y };
}

template<typename CoordsType>
struct AlgTestResult
{
    using PathType = std::vector<pf::Point<CoordsType>>;

    AlgTestResult( std::string algName, long durationMicroS )
            : algName( algName ), durationMicroS( durationMicroS )
    {}

    AlgTestResult( std::string algName, long durationMicroS, PathType& path )
            : algName( algName ), durationMicroS( durationMicroS ), path( path )
    {}

    std::string algName;
    long durationMicroS;
    PathType path;
};

template<typename MapType>
std::vector<AlgTestResult<uint16_t>> ComparePathAlgs( MapType& map, int16_t startX, int16_t startY,
                                                      int16_t targetX, int16_t targetY )
{
    std::vector<AlgTestResult<uint16_t>> result;

    MeasureUtils::Settings measureSettings( 300, 30 );


    pf::AStar16 searcherAStar( map );
    auto evalResultAstar16 = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings,
                                                                                             &pf::AStar16::find,
                                                                                             &searcherAStar,
                                                                                             startX, startY, targetX,
                                                                                             targetY,
                                                                                             1u );

    result.push_back( AlgTestResult<uint16_t>( "Simple A*", evalResultAstar16.time, evalResultAstar16.result ));

    pf::AStar16GoalBounded searcherAStarGoalBounded( map );
    auto evalResultAStar16GoalBounded = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings,
                                                                                                        &pf::AStar16GoalBounded::find,
                                                                                                        &searcherAStarGoalBounded,
                                                                                                        startX, startY,
                                                                                                        targetX,
                                                                                                        targetY,
                                                                                                        1u );

    result.push_back( AlgTestResult<uint16_t>( "A* + Goal bound", evalResultAStar16GoalBounded.time,
                                               evalResultAStar16GoalBounded.result ));

    pf::JPSAStar16 searcherJps( map );
    auto evalResultJps = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings,
                                                                                         &pf::JPSAStar16::find,
                                                                                         &searcherJps,
                                                                                         startX, startY, targetX,
                                                                                         targetY,
                                                                                         1u );

    result.push_back( AlgTestResult<uint16_t>( "JPS A*", evalResultJps.time, evalResultJps.result ));

    pf::JPSAStar16GBound searcherJpsGBound( map );
    auto evalResultJpsGBound = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings,
                                                                                               &pf::JPSAStar16GBound::find,
                                                                                               &searcherJpsGBound,
                                                                                               startX, startY, targetX,
                                                                                               targetY,
                                                                                               1u );

    result.push_back(
            AlgTestResult<uint16_t>( "JPS A* + Goal bound", evalResultJpsGBound.time, evalResultJpsGBound.result ));

    JPS::Searcher<MapType> search( map );

    JPS::Position start = { startX, startY };
    JPS::Position end = { targetX, targetY };

    auto refEvalResult = MeasureUtils::measureMethodEvalTime<std::chrono::microseconds>( measureSettings,
                                                                                         &JPS::Searcher<MapType>::findPathTest,
                                                                                         &search,
                                                                                         start, end, 1 );

//    result.push_back( AlgTestResult<uint16_t>( "JPS A* Ref", refEvalResult.time ));

    return result;
}

class AlgHelper
{
public:

    virtual void InitAlg( const pf::SimpleMap<uint16_t>* map ) = 0;

    virtual void Calc( sf::Vector2i start, sf::Vector2i goal )
    {
        m_result = m_searcher->find( start.x, start.y, goal.x, goal.y, 1 );
    }

    virtual void Draw( DisplayedMap<uint16_t>& dMap )
    {
        dMap.AddPath( m_result );
    }

    void SetDbgInfoEnabled( bool enabled )
    {
        m_dbgInfoEnabled = enabled;
    }

protected:

    pf::PathFinder<uint16_t>* m_searcher{ nullptr };
    std::vector<pf::Point<uint16_t>> m_result;
    bool m_dbgInfoEnabled{ false };
};

class AStarAlgHelper : public AlgHelper
{
public:

    virtual void InitAlg( const pf::SimpleMap<uint16_t>* map ) override
    {
        m_searcher = new pf::AStar16( *map );
    }

    virtual void Draw( DisplayedMap<uint16_t>& dMap ) override
    {
        if( m_dbgInfoEnabled )
        {
            dMap.AddDbgInfo( *static_cast<pf::AStar16*>(m_searcher));
        }

        AlgHelper::Draw( dMap );
    }
};

class AStarGBAlgHelper : public AlgHelper
{
public:

    virtual void InitAlg( const pf::SimpleMap<uint16_t>* map ) override
    {
        m_searcher = new pf::AStar16GoalBounded( *map );
    }

    virtual void Draw( DisplayedMap<uint16_t>& dMap ) override
    {
        if( m_dbgInfoEnabled )
        {
            dMap.AddDbgInfo( *static_cast<pf::AStar16GoalBounded*>(m_searcher));
        }

        AlgHelper::Draw( dMap );
    }
};

class JPSAlgHelper : public AlgHelper
{
public:

    virtual void InitAlg( const pf::SimpleMap<uint16_t>* map ) override
    {
        m_searcher = new pf::JPSAStar16Dbg( *map );
    }

    virtual void Draw( DisplayedMap<uint16_t>& dMap ) override
    {
        if( m_dbgInfoEnabled )
        {
            dMap.AddDbgInfo( *static_cast<pf::JPSAStar16Dbg*>(m_searcher));
        }

        AlgHelper::Draw( dMap );
    }
};

class JPSGBAlgHelper : public AlgHelper
{
public:

    virtual void InitAlg( const pf::SimpleMap<uint16_t>* map ) override
    {
        m_searcher = new pf::JPSAStar16GBoundDbg( *map );
    }

    virtual void Draw( DisplayedMap<uint16_t>& dMap ) override
    {
        if( m_dbgInfoEnabled )
        {
            dMap.AddDbgInfo( *static_cast<pf::JPSAStar16GBoundDbg*>(m_searcher));
        }

        AlgHelper::Draw( dMap );
    }
};

int main( int argc, char* argv[] )
{
    sf::RenderWindow renderWindow( sf::VideoMode( 1600, 900 ), "Path finding visualiser" );
    renderWindow.resetGLStates();
    renderWindow.setFramerateLimit( 75 );

    sfg::SFGUI sfgui;
    renderWindow.setActive();

    auto sfml_window = sfg::Window::Create();
    sfml_window->SetTitle( "Map" );
    sfml_window->SetPosition( sf::Vector2f( 0, 0 ));
    sfml_window->SetStyle( sfg::Window::Style::NO_STYLE );

    auto sfml_canvas = sfg::Canvas::Create();
    sfml_window->Add( sfml_canvas );
    sfml_canvas->SetRequisition( sf::Vector2f( renderWindow.getSize().x - 230, renderWindow.getSize().y - 50 ));


    auto guiWindow = sfg::Window::Create();
    guiWindow->SetPosition( sf::Vector2f( sfml_canvas->GetRequisition().x + 30, 0 ));
    guiWindow->SetRequisition( sf::Vector2f( 200, 500 ));

    auto box = sfg::Box::Create( sfg::Box::Orientation::VERTICAL, 5.f );

    auto fpsLbl = sfg::Label::Create( "Fps: " );
    auto startPointLbl = sfg::Label::Create( "Start: " );
    auto targetPointLbl = sfg::Label::Create( "Target: " );
//    auto pathLengthLbl = sfg::Label::Create( "Path length: " );
//    auto searchTimeLbl = sfg::Label::Create( "Time: " );
    box->Pack( fpsLbl, false );
    box->Pack( startPointLbl, false );
    box->Pack( targetPointLbl, false );
//    box->Pack( pathLengthLbl, false );
//    box->Pack( searchTimeLbl, false );

    auto selectHeuristic = sfg::ComboBox::Create();

    for( auto item : { "ManhattanDistance", "StraightDistance", "TieBreakerDistance" } )
    {
        selectHeuristic->AppendItem( item );
    }
//    box->Pack( selectHeuristic, false );

    auto compareAlgsBtn = sfg::Button::Create( "Run comparison!" );
    box->Pack( compareAlgsBtn, false );

    auto compareRes = sfg::Label::Create();

    box->Pack( compareRes, false );

    guiWindow->Add( box );

    sfg::Desktop desktop;
    desktop.Add( sfml_window );
    desktop.Add( guiWindow );

    char buf[256];

    sf::Font font;

    // Load from a font file on disk
    if( !font.loadFromFile( "calibri.ttf" ))
    {
        // Error...
    }

    char const * const filter = "*.map";
    char const* lTheOpenFileName;
    lTheOpenFileName = tinyfd_openFileDialog (
            "Choose a map.",
            "",
            1,
            &filter,
            NULL,
            0);

    if( lTheOpenFileName == nullptr )
    {
        return 0;
    }

    auto map = loaders::load_map_ai_lab_format<uint16_t>( lTheOpenFileName );

    if( map == nullptr )
    {
        return 0;
    }

    map->CalculateGoalBounds();

    pf::GoalBounding::FFMap ffMap( *map );

    bool dbgEnabled{ false };
    size_t currentHelper = 0;

    std::vector<std::unique_ptr<AlgHelper>> helpers;
    helpers.emplace_back( new AStarAlgHelper());
    helpers.emplace_back( new AStarGBAlgHelper());
    helpers.emplace_back( new JPSAlgHelper());
    helpers.emplace_back( new JPSGBAlgHelper());

    for( auto& helper : helpers )
    {
        helper->InitAlg( map );
    }

    DisplayedMap<uint16_t> displayedMap( *map );
    displayedMap.Init();
    displayedMap.SetFFMap( &ffMap );

    sf::VertexArray mousePosQuad( sf::Quads, 4 );
    sf::VertexArray startPosQuad( sf::Quads, 4 );
    sf::VertexArray targetPosQuad( sf::Quads, 4 );

    sf::Vector2i startCoords;
    sf::Vector2i targetCoords;

    bool ignoreEvents = false;

    compareAlgsBtn->GetSignal( sfg::Button::OnLeftClick )
            .Connect( [&]()
                      {
                          ignoreEvents = true;

                          auto result = ComparePathAlgs( *map, startCoords.x, startCoords.y, targetCoords.x,
                                                         targetCoords.y );

                          std::stringstream str;

                          for( const auto& algTestResult : result )
                          {
                              str << algTestResult.algName << ": " << algTestResult.durationMicroS << "\n";
                          }

                          compareRes->SetText( str.str());
                      } );

    const float moveSpeed = 2;
    const float scaleSpeed = 1.1;
    float scale = 1.f;
    bool scaled = false;

    sf::Clock clock;

    while( renderWindow.isOpen())
    {
        float scaleDelta = 1.f;

        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while( renderWindow.pollEvent( event ))
        {
            ignoreEvents = false;
            desktop.HandleEvent( event );

            if(ignoreEvents)
            {
                break;
            }

            // "close requested" event: we close the window
            if( event.type == sf::Event::Closed )
            {
                renderWindow.close();
            }

            if( event.type == sf::Event::Resized )
            {

//                sfml_canvas->SetRequisition(
//                        sf::Vector2f( renderWindow.getSize().x - 230, renderWindow.getSize().y - 50 ));

                guiWindow->SetPosition( sf::Vector2f( renderWindow.getSize().x - 200, 0 ));
            }

            if( event.type == sf::Event::MouseWheelScrolled )
            {
                if( event.mouseWheelScroll.delta > 0 )
                {
                    scaleDelta /= scaleSpeed;
                }
                else
                {
                    scaleDelta *= scaleSpeed;
                }
                scale *= scaleDelta;
                scaled = true;
            }

            if( event.type == sf::Event::MouseButtonReleased )
            {
                if( !sfml_canvas->GetAllocation().contains( event.mouseButton.x, event.mouseButton.y ))
                {
                    continue;
                }

                displayedMap.SetDrawFFMap( false );

                if( event.mouseButton.button == sf::Mouse::Button::Left )
                {
                    targetCoords = get_tile_under_mouse( renderWindow, sfml_canvas, *map );
                }
                if( event.mouseButton.button == sf::Mouse::Button::Right )
                {
                    startCoords = get_tile_under_mouse( renderWindow, sfml_canvas, *map );
                }

                if( sf::Keyboard::isKeyPressed( sf::Keyboard::LControl ))
                {

                    displayedMap.DrawBoundingBoxes( targetCoords.x, targetCoords.y );
                }
                else if( sf::Keyboard::isKeyPressed( sf::Keyboard::LShift ))
                {
                    pf::GoalBounding::FloodFill( *map, ffMap, targetCoords.x, targetCoords.y, 1 );
                    displayedMap.UpdateFFMap();
                    displayedMap.SetDrawFFMap( true );
                }
                else
                {
                    for( auto& helper : helpers )
                    {
                        helper->Calc( startCoords, targetCoords );
                    }

                    displayedMap.ClearOverlay();
                    helpers[currentHelper]->Draw( displayedMap );
                    displayedMap.Draw();

                    startPointLbl->SetText(
                            ( boost::format( "Start: (%d, %d)" ) % startCoords.x % startCoords.y ).str());
                    targetPointLbl->SetText(
                            ( boost::format( "Target: (%d, %d)" ) % targetCoords.x % targetCoords.y ).str());
//                    pathLengthLbl->SetText(( boost::format( "Path length: %d" ) % result.result.size()).str());
//                    searchTimeLbl->SetText(( boost::format( "Time: %d" ) % result.time ).str());
                }

            }

            if( event.type == sf::Event::KeyReleased )
            {
                if( event.key.code == sf::Keyboard::Key::Numpad4 )
                {
                    currentHelper--;
                    currentHelper = currentHelper > helpers.size() ? helpers.size() - 1 : currentHelper;
                }

                if( event.key.code == sf::Keyboard::Key::Numpad6 )
                {
                    currentHelper = ( currentHelper + 1 ) % helpers.size();
                }

                if( event.key.code == sf::Keyboard::Key::Numpad5 )
                {
                    dbgEnabled = !dbgEnabled;
                    for( auto& helper : helpers )
                    {
                        helper->SetDbgInfoEnabled( dbgEnabled );
                    }
                }
            }


            displayedMap.ClearOverlay();
            helpers[currentHelper]->Draw( displayedMap );
            displayedMap.Draw();
        }

        auto coords = get_tile_under_mouse( renderWindow, sfml_canvas, *map );
        fill_quad( &mousePosQuad[0], coords.x, coords.y, sf::Color::Magenta, k_quadSize );

        fill_quad( &startPosQuad[0], startCoords.x, startCoords.y, sf::Color::White, k_quadSize );
        fill_quad( &targetPosQuad[0], targetCoords.x, targetCoords.y, sf::Color::White, k_quadSize );

        float scaledMS = moveSpeed * scale;
        float upDelta = -scaledMS * sf::Keyboard::isKeyPressed( sf::Keyboard::Up );
        float downDelta = scaledMS * sf::Keyboard::isKeyPressed( sf::Keyboard::Down );
        float leftDelta = -scaledMS * sf::Keyboard::isKeyPressed( sf::Keyboard::Left );
        float rightDelta = scaledMS * sf::Keyboard::isKeyPressed( sf::Keyboard::Right );

        desktop.BringToFront(guiWindow);

        auto elapsedTime = clock.restart();
        desktop.Update( elapsedTime.asSeconds());

        // clear the window with black color
        renderWindow.clear( sf::Color::Black );

        sfml_canvas->Bind();

        auto view = sfml_canvas->GetView();
        view.move( leftDelta + rightDelta, upDelta + downDelta );
        if( scaled )
        {
            view.zoom( scaleDelta );
            scaled = false;
        }

        sfml_canvas->SetView( view );

        sfml_canvas->Clear( sf::Color::Black );

        sfml_canvas->Draw( displayedMap );
        sfml_canvas->Draw( mousePosQuad );
        sfml_canvas->Draw( startPosQuad );
        sfml_canvas->Draw( targetPosQuad );

        sf::Text text;

        std::snprintf( buf, 256, "Fps: %.1f", 1 / elapsedTime.asSeconds());
        fpsLbl->SetText( buf );

        sfml_canvas->Draw( text );

        sfml_canvas->Display();
        sfml_canvas->Unbind();

        renderWindow.setActive( true );

        sfgui.Display( renderWindow );

        // end the current frame
        renderWindow.display();
    }

    delete map;

    return 0;
}
